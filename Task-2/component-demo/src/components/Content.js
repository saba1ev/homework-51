import React from 'react';
import './css/Content.css';

const Content = () =>{
  return(
    <div className='content'>
      <div>
        <h3>Toyota ist</h3>
        <div className='ist'></div>
        <p>Впервые Toyota Ist, компакт-кар с вместительным багажником и спортивным
          дизайном салона, дебютировал в качестве прототипа на мотор-шоу в Токио в 2001 году.
          В Японии продажи этого автомобиля начались в мае 2002 года, а с весны 2003
          года Toyota Ist стал продаваться в США под маркой Scion. В Европе этот автомобиль появился только в 2004 году.</p>
      </div>
      <div>
        <h3>Toyota Corolla</h3>
        <div className='corolla'></div>
        <p>Первая Toyota Corolla была представлена в Японии в октябре 1966 года — это был маленький,
          длиной всего 3,85 м, заднеприводный двухдверный седан.
          Четырехцилиндровый двигатель объемом 1,1 литра развивал 60 л. с., коробка передач была четырехступенчатой,
          задняя подвеска — рессорной зависимой, а передняя — независимой с одной поперечной рессорой (позже пружинной).
          В мае 1967 года появились модификация с четырехдверным кузовом, а также трехдверный универсал.
          Еще через год в продажу поступило купе Corolla Sprinter. Тогда же начались продажи в США.</p>
      </div>
      <div>
        <h3>Toyota Crown</h3>
        <div className='crown'></div>
        <p>Дебютировавший в 1955 году новый «коронованный» седан Crown (заводской код S30)
          достаточно быстро обрел популярность – помимо использования в качестве такси,
          автомобиль пополнил парки различных госструктур, стал рабочей машиной для представителей среднего бизнеса,
          и даже применялся в некоторых префектурах Японии как полицейское авто.</p>
      </div>
    </div>
  )
};

export default Content;