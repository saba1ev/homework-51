import React from 'react';
import './css/Header.css';

const Header = () =>{
  return(
    <div className='bg'>
      <div className='container'>
        <div className='header'>
          <a href="#" className='logo'></a>
          <nav>
            <ul className='ul-nav'>
              <li><a href="#">Home</a></li>
              <li><a href="#">Contact</a></li>
              <li><a href="#">About Us</a></li>
            </ul>
          </nav>
        </div>


      </div>
    </div>

  )
};

export default Header;