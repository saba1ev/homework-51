import React from 'react';
import './css/Footer.css';

const Footer = () =>{
  return(
    <div className='bg-fot'>
      <div className='container'>
        <div className='icons'>
          <a className='vk' href="#"></a>
          <a className='inst' href="#"></a>
          <a className='fb' href="#"></a>
        </div>
      </div>
    </div>
  )
};

export default Footer;