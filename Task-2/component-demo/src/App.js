import React, { Component, Fragment } from 'react';
import './App.css';
import Header from './components/Header';
import Content from "./components/Content";
import SideBar from "./components/SideBar";
import Footer from "./components/Footer";
class App extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        <div className='container'>
          <div className='flex'>
            <Content/>
            <SideBar/>
          </div>
        </div>
        <Footer/>
      </Fragment>

    )
  }
}

export default App;
