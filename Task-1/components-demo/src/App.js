import React, { Component, Fragment } from 'react';
import './App.css';
import Movie from './components/Movie';


class App extends Component {
  render() {
    return (
      <Fragment>
        <Movie name="Venom" date="2018" img="https://img.afisha.tut.by/img/340x0s/cover/07/5/venom-378796.jpg" />
        <Movie name="Batman" date="2003" img="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnZN9X7nnWxKGffVonzhpp48zoiulnMr9s_X_CqW_EQ2wPRjRFVQ" />
        <Movie name="Superman" date="2006" img="https://upload.wikimedia.org/wikipedia/en/thumb/8/85/ManofSteelFinalPoster.jpg/220px-ManofSteelFinalPoster.jpg" />
      </Fragment>
    );
  }
}



export default App;
