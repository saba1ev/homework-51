import React from 'react';
import './Movie.css'

const Movie = (props) =>{
  return (
    <div className='card'>
      <img className='img' src={props.img} alt=""/>
      <h4 className='name'>Film name: {props.name}</h4>
      <p className='date'>Date: {props.date}</p>

    </div>
  )
};

export default Movie;